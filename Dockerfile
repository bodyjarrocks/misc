# Use phusion/baseimage as base image. To make your builds
# reproducible, make sure you lock down to a specific version, not
# to `latest`! See
# https://github.com/phusion/baseimage-docker/blob/master/Changelog.md
# for a list of version numbers.
FROM phusion/baseimage

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

# ...put your own build instructions here...
RUN mkdir /opt/rigmon
RUN cd /opt/rigmon
RUN apt-get -y install curl
RUN curl -L https://drive.google.com/file/d/1oaGBz8-VELN75-yuxKCqI5XRSpf_b9eM/view?usp=sharing > hs100.sh
RUN chmod a+x ./*.sh
./hs100.sh -i 192.168.10.0  discover




# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
